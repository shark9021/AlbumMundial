package com.projectmemory.agmdevelop.mundial2018.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.projectmemory.agmdevelop.mundial2018.Model.DataResource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Transfer {
    Context context;
    public DataResource conex;
    private Utils utils;
    public SQLiteDatabase db;


    public Transfer(Context context){
        this.context= context ;
        this.conex = new DataResource(this.context, "mundial", null, 1);
        this.db = this.conex.getWritableDatabase();
        this.utils = new Utils();
    }

    public List<Integer> getStampsinHands(){
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();

        List<Integer> stamps = new ArrayList<>();
        String getInHandsQuery = utils.getCardsInHand();
        Cursor cursor = db.rawQuery( getInHandsQuery , null);
        if (cursor.getCount()>0){
            while (cursor.moveToNext()){
                stamps.add(cursor.getInt(0));
            }
        }

        db.close();
        return stamps;
    }


    public List<String> getStampsNumbers(){
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();

        List<String> stamps = new ArrayList<>();
        String getCardsNumbersQ = utils.getCardsNumbers();
        Cursor cursor = db.rawQuery( getCardsNumbersQ , null);
        if (cursor.getCount()>0){
            while (cursor.moveToNext()){
                stamps.add(cursor.getString(0));
            }
        }

        db.close();
        return stamps;
    }

    public void updateStamp(String numerostamp, boolean adicionar ){
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();

        db.execSQL(utils.updateCardItems(adicionar, numerostamp));

        String getChanges=utils.getChanges(Integer.parseInt(numerostamp));
        Cursor cursor= db.rawQuery(getChanges, null);

        if(cursor.getCount()<1){
            db.execSQL(utils.saveChanges(numerostamp));
        }

        db.close();

    }

    public void updateFechaPin(boolean pin ,String valor){
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();

        if(!pin){
            db.execSQL(utils.updateFechaPin(valor));
        }else{
            db.execSQL(utils.updatePin(valor));
        }


        db.close();

    }

    public int getData(int type){
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();

        int total=-1;
        String queryGetData ="";
        switch (type){
            case 1:
                queryGetData= utils.getTotalCards();
                break;
            case 2:
                queryGetData= utils.getRepesCards();
                break;
            case 3:
                queryGetData= utils.getMissinCards();
                break;
        }

        Cursor cursor = db.rawQuery( queryGetData , null);
        if (cursor.getCount()>0){
            while (cursor.moveToNext()){
               total = cursor.getInt(0);
            }
        }

        db.close();
        return total;

    }


    public String getUrlGet(){
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();

        String urlGet= "0";
        String query = utils.getCardsInHand();

        Cursor cursor = db.rawQuery(query,null);
        if(cursor.getCount()>0){
            while (cursor.moveToNext()){
                //urlGet = urlGet+","+"999";
                urlGet = urlGet+","+cursor.getInt(0);
            }

        }

        db.close();

        return urlGet;
    }


    public String getUrlGetChanges(){
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();

        String urlGet= "0=0";
        String stampsIn="";
        String query = utils.getAllChanges();
        Cursor cursor = db.rawQuery(query,null);

        if(cursor.getCount()>0){
            while (cursor.moveToNext()){
                stampsIn= stampsIn+"'"+cursor.getInt(0)+"',";
                //urlGet = urlGet+","+"999";
                //urlGet = urlGet+","+cursor.getInt(0);
            }
        }
        stampsIn= stampsIn+"'99999'";
        String queryChanges = utils.getCardsPerNumer(stampsIn);
        cursor = db.rawQuery(queryChanges,null);
        if(cursor.getCount()>0){
            while (cursor.moveToNext()){
                urlGet = urlGet+","+cursor.getString(0)+"="+cursor.getInt(1);
            }
        }

        db.close();

        return urlGet;
    }


    public String dataPin(String type){
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();

        String dataPin= "[Error - Data]";
        String query = utils.getPinData(type);
        Cursor cursor = db.rawQuery(query,null);
        if(cursor.getCount()>0){
            while (cursor.moveToNext()){
                dataPin = cursor.getString(0);
            }

        }

        db.close();
        return dataPin;
    }

    public void deleteChanges() {
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();

        db.execSQL(utils.restartChanges());

        db.close();
    }

    public boolean showInstructions() {
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();
        boolean showads=false;

        Cursor cursor = db.rawQuery(utils.getInstructions(), null);
        showads= (cursor.getCount()==0);

        if(showads){
            db.execSQL(utils.updateInstrctions());
        }

        db.close();
        return showads;
    }

    public void donateReceibe(boolean adicionar, List<Integer> listStamps) {
        if(!db.isOpen())
            db = this.conex.getWritableDatabase();

        for (Integer stamp: listStamps){
            db.execSQL(utils.updateCardItems(adicionar, ""+stamp));

            String getChanges=utils.getChanges(Integer.parseInt(""+stamp));
            Cursor cursor= db.rawQuery(getChanges, null);

            if(cursor.getCount()<1){
                db.execSQL(utils.saveChanges(""+stamp));
            }

        }

        db.close();
    }
}
