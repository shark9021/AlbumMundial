package com.projectmemory.agmdevelop.mundial2018.Utils;

public class Utils {


    public String saveCard(int stamp){
        String query = "INSERT INTO INVENTORY (estamp, inhand) VALUES('"+stamp+"', 0)";
        return query;
    }
    public String updateInstrctions(){
        String query = "UPDATE FLAG_COMPARE SET status=1";
        return query;
    }

    public String getInstructions(){
        String query = "SELECT  status FROM  FLAG_COMPARE WHERE status=1";
        return query;
    }

    public String getCardsInHand(){
        String query = "SELECT inhand FROM INVENTORY";
        return query;
    }

    public String getCardsNumbers(){
        String query = "SELECT estamp FROM INVENTORY";
        return query;
    }

    public String getCardsPerNumer(String stampsNumber){
        String query = "SELECT estamp, inhand FROM INVENTORY WHERE estamp IN ("+stampsNumber+")";
        return query;
    }

    public String updateFechaPin(String fecha){
        String query = "UPDATE PIN SET generated='"+fecha+"'";
        return query;
    }
    public String updatePin(String pin){
        String query = "UPDATE PIN SET pin='"+pin+"'";
        return query;
    }

    public String getChanges(Integer stamp){
        String query = "SELECT * FROM CHANGES WHERE estamp="+stamp;
        return query;
    }

    public String restartChanges(){
        String query = "DELETE FROM CHANGES";
        return query;
    }

    public String getAllChanges(){
        String query = "SELECT * FROM CHANGES";
        return query;
    }

    public String saveChanges(String stamp){

        String query="INSERT INTO CHANGES (estamp) VALUES("+stamp+")";
        return query;

    }

    public String updateCardItems(boolean sumar, String stamp){
        String query="false";
        if(sumar){
             query = "UPDATE INVENTORY SET inhand=inhand+1 WHERE estamp='"+stamp+"'";
        }else{
             query = "UPDATE INVENTORY SET inhand=inhand-1 WHERE estamp='"+stamp+"'";
        }
        return query;
    }
    public String getTotalCards(){
        String query = "SELECT COUNT(*) FROM INVENTORY WHERE inhand>0";
        return query;
    }
    public String getMissinCards(){
        String query = "SELECT COUNT(*) FROM INVENTORY WHERE inhand=0";
        return query;
    }

    public String getRepesCards(){
        String query = "SELECT COUNT(*) FROM INVENTORY WHERE inhand>1";
        return query;
    }

    public String getPinData(String element){
        String query = "SELECT "+element+" FROM  PIN";
        return query;
    }

    //////////////////////INITS////////////////////////////////////////////////



    public final static String CREATE_TABLE_INVENTORY =
            "CREATE TABLE INVENTORY (" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT, estamp VARCHAR(5) , inhand INTEGER)";



    public final static String CREATE_TABLE_PIN =
            "CREATE TABLE PIN (" +
                    "pin VARCHAR(100), generated VARCHAR(100))";

    public final static String CREATE_TABLE_CHANGES =
            "CREATE TABLE CHANGES (" +
                    "estamp INTEGER)";

    public final static String CREATE_TABLE_FLAG_COMPARE=
            "CREATE TABLE FLAG_COMPARE (" +
                    "status INTEGER)";

}


