package com.projectmemory.agmdevelop.mundial2018.Utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.projectmemory.agmdevelop.mundial2018.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class AdapterGrid extends BaseAdapter {

    private Context context;
    private int layout;
    private List<String> items;
    public List<String> itemsMostrados;
    public List<Integer> stampInhand;
    public List<Integer> stampInhandMostrados;


    public AdapterGrid(Context contexto, int layout, List<String> itemsendigs, List<Integer>  stampInhand) {

        this.context = contexto;
        this.layout = layout;
        this.items = itemsendigs;
        this.stampInhand = stampInhand;

        itemsMostrados = new ArrayList<>();
        for (String object: itemsendigs) {
            itemsMostrados.add(object);
        }

        stampInhandMostrados = new ArrayList<>();
        for (Integer object: stampInhand) {
            stampInhandMostrados.add(object);
        }
    }

    public void updateGrid(List<String> itemsendigs, List<Integer>  stampInhandSendings, String texto){
        itemsMostrados.clear();
        stampInhandMostrados.clear();
        stampInhand.clear();

        this.items = itemsendigs;

        for (String object: itemsendigs) {
            itemsMostrados.add(object);
        }

        for (Integer object: stampInhandSendings) {
            stampInhandMostrados.add(object);
        }
        for (Integer object: stampInhandSendings) {
            stampInhand.add(object);
        }

       filtrar(texto);
    }

    public void filtrar(String texto){

        itemsMostrados.clear();
        stampInhandMostrados.clear();
        int possearch;
        int index=0;

        for (String object: this.items) {
            possearch=object.indexOf(texto);
            if(possearch > -1){
                stampInhandMostrados.add(stampInhand.get(index));
                itemsMostrados.add(object);
            }
            index++;
        }

        notifyDataSetChanged();
    }

    public void filtrarCantidad(int cantidad){

        int index =0;
        itemsMostrados.clear();
        stampInhandMostrados.clear();
        /////// ! = ok | 2 : repetidas, 3: faltantes
        switch (cantidad){
            case 1:
                for (Integer inhand: this.stampInhand) {
                    if(inhand==1){
                        itemsMostrados.add(this.items.get(index));
                        stampInhandMostrados.add(stampInhand.get(index));
                    }
                    index++;
                }
                break;
            case 2:
                for (Integer inhand: this.stampInhand) {
                    if(inhand>1){
                        itemsMostrados.add(this.items.get(index));
                        stampInhandMostrados.add(stampInhand.get(index));
                    }
                    index++;
                }
                break;
            case 3:
                for (Integer inhand: this.stampInhand) {
                    if(inhand==0){
                        itemsMostrados.add(this.items.get(index));
                        stampInhandMostrados.add(stampInhand.get(index));
                    }
                    index++;
                }
                break;

        }

        notifyDataSetChanged();
    }

    public void mostrarTodas(){
        int index =0;
        itemsMostrados.clear();
        stampInhandMostrados.clear();
        for (Integer inhand: this.stampInhand) {

                itemsMostrados.add(this.items.get(index));
                stampInhandMostrados.add(stampInhand.get(index));

            index++;
        }
        notifyDataSetChanged();
    }

    public int getInhandTouched(int pos){
        return stampInhandMostrados.get(pos);
    }


    @Override
    public int getCount() {
        return this.itemsMostrados.size() ;
    }

    @Override
    public Object getItem(int i) {
        return itemsMostrados.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup viewGroup) {

        ViewHolder holder;
        /////PATRO VIEW HOLDER
        if(convertView==null){
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);
            convertView = layoutInflater.inflate(R.layout.grid_stamp, null);
            holder = new ViewHolder();
            holder.numeroestampilla = convertView.findViewById(R.id.numberstamp);
            holder.numeroCopias = convertView.findViewById(R.id.txtoh);
            holder.fondoStamps = convertView.findViewById(R.id.imgfondostamp);
            convertView.setTag(holder);

        }else{

            holder = (ViewHolder) convertView.getTag();
        }

        //// esto reemplaza el getItem
        String numberStamp = itemsMostrados.get(pos);
        int stampInHandNb = stampInhandMostrados.get(pos);

        if(stampInHandNb>1){
            holder.fondoStamps.setImageResource(R.drawable.repetidas);
        }else if(stampInHandNb==1){

            holder.fondoStamps.setImageResource(R.drawable.okey);
        }else{
            holder.fondoStamps.setImageResource(R.drawable.faltantes);

        }


        holder.numeroestampilla.setText(numberStamp);
        holder.numeroCopias.setText(""+stampInHandNb);

        return convertView;

    }



    static  class ViewHolder{
        private TextView numeroestampilla;
        private TextView numeroCopias;
        private ImageView fondoStamps;
    }



}
