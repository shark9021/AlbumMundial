package com.projectmemory.agmdevelop.mundial2018;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.projectmemory.agmdevelop.mundial2018.Utils.Transfer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompareActivity extends AppCompatActivity {
    TextView txtPin, statusPin, txtAmigoFaltantes, txtLocalfaltantes;
    Button btnpin, btnCompare;
    Transfer transfer;
    String supraPin, supraGenerate, parametros;
    Boolean supraExistPin, supraDialog=false, supraCompare=false;
    List< Integer >  inhand;
    List< Integer > inhandLocal;
    List< Integer > listDonate, listRecibe;
    InterstitialAd mInterstitialAd;
    AdRequest adRequestInters;
    String JsonURL = "http://invictaperu.pe/scr1pt/1713d3v3l0p.php";
    StringRequest eventoReq;
    RequestQueue queue;
    EditText pinAmigo;
    Boolean supraAdsShowed = false, hayInternet=false;
    ImageView btnObtenerTodas, btnDonarTodas;
    ImageView imgHelper;
    AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_compare);
        ////////////////////////////////////ANTIFOCUS///////////////////////////////////////////////
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ////////////////////////////////////////ADS////////////////////////////////////////////////
        MobileAds.initialize(this,
                "ca-app-pub-7375783119283606~7534484409");
        mAdView= new AdView(this);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mInterstitialAd= new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7375783119283606/8655994382");
        loadInterstitial();

        mInterstitialAd.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

                supraAdsShowed = false ;
                loadInterstitial();
            }

            @Override
            public void onAdOpened() {
                supraAdsShowed = true;

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {

            }

        });
        /////////////////////////////////INITS//////////////////////////////////////////////////////
        hayInternet = isNetworkAvailable(this);



        transfer = new Transfer(this);
        inhandLocal = new ArrayList<Integer>();
        listDonate = new ArrayList<Integer>();
        listRecibe = new ArrayList<Integer>();
        inhand = new ArrayList<Integer>();
        btnpin = findViewById(R.id.btnGeneratePin);
        btnCompare=findViewById(R.id.btncomparar);
        txtPin= findViewById(R.id.txtPin);
        txtLocalfaltantes = findViewById(R.id.txtCruzeAmigo);
        txtAmigoFaltantes = findViewById(R.id.txtCruzeLocal);
        statusPin = findViewById(R.id.txtFechaPin);
        pinAmigo = findViewById(R.id.edtpinamigo);

        btnObtenerTodas= findViewById(R.id.btnObtenerTodas);
        btnDonarTodas= findViewById(R.id.btnDonadas);

        btnObtenerTodas.setVisibility(View.INVISIBLE);
        btnDonarTodas.setVisibility(View.INVISIBLE);

        imgHelper=findViewById(R.id.imgHelper);

        pinAmigo.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5), new InputFilter.AllCaps()});

        inhandLocal = transfer.getStampsinHands();

        //////////////////////////////////////INSTRUCTIONS//////////////////////////////////////////
        if(transfer.showInstructions()){
            MostrarInstrucciones();
        }

        imgHelper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MostrarInstrucciones();
            }
        });


        ////////////////////////////////////////INTERNET////////////////////////////////////////////
        if(!hayInternet){
            Toast.makeText(this, getResources().getString(R.string.internet), Toast.LENGTH_LONG).show();
        }

        //////////////////////////////////////EVALUATE PIN//////////////////////////////////////////


        supraPin = transfer.dataPin("pin");

        if(supraPin.equals("NN")){
            supraExistPin = false;
            txtPin.setText(getResources().getText(R.string.TextPinGen));
            statusPin.setText(getResources().getText(R.string.never));

        }else{
            supraGenerate= transfer.dataPin("generated");
            supraExistPin = true;
            txtPin.setText(supraPin);
            statusPin.setText(supraGenerate);
        }

        //////////////////////////VOYLLEY HTTP /////////////////////////////////////////////////////
        queue= Volley.newRequestQueue(this);

        //////////////////////////DONATE AND RECEIBE ///////////////////////////////////////////////
        btnObtenerTodas.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                updateAfterCruze(true, listRecibe);
            }
        });

        btnDonarTodas.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                updateAfterCruze(false, listDonate);
            }
        });

        /////////////////////////////BTN GENERATEPIN////////////////////////////////////////////////
        btnpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(hayInternet){

                if(!supraAdsShowed)
                    mInterstitialAd.show();

                supraCompare=false;
                parametros = transfer.getUrlGet();
                if(supraExistPin){
                    CALLHTPP("update");
                }else{
                    CALLHTPP("new");
                }
                btnpin.setEnabled(false);
                ShowProcessing();
            }else{
                Toast.makeText(CompareActivity.this, getResources().getString(R.string.internet), Toast.LENGTH_LONG).show();
                hayInternet = isNetworkAvailable(CompareActivity.this);

            }


            }
        });


        btnCompare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(hayInternet) {

                    if (!supraAdsShowed)
                        mInterstitialAd.show();

                    supraCompare = true;
                    String txpinAmigo = pinAmigo.getText().toString();
                    inhand.clear();
                    if (!txpinAmigo.equals("")) {
                        if (txpinAmigo.length() < 5) {
                            Toast.makeText(CompareActivity.this, getResources().getString(R.string.badpin), Toast.LENGTH_SHORT).show();
                        } else {

                            parametros = txpinAmigo;
                            CALLHTPP("compare");
                            btnCompare.setEnabled(false);
                            ShowProcessing();

                        }

                    }
                }else{
                    Toast.makeText(CompareActivity.this, getResources().getString(R.string.internet), Toast.LENGTH_LONG).show();
                    hayInternet = isNetworkAvailable(CompareActivity.this);
                }

            }
        });

    }

    public boolean isNetworkAvailable(final Context context) {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    private void ShowProcessing() {
        Toast.makeText(this, getResources().getString(R.string.processing), Toast.LENGTH_LONG).show();
    }

    private void MostrarInstrucciones() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View customView = LayoutInflater.from(this).inflate(
                R.layout.instructions_compare, null, false);


        builder.setView(customView);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });

        final AlertDialog mdialog = builder.show();

        TextView instrucciones = customView.findViewById(R.id.txtInstructions);
        instrucciones.setText(Html.fromHtml("<b>1.</b> ")
                        +getResources().getString(R.string.instructone)
                        +Html.fromHtml("<br><br>")
                         +Html.fromHtml("<b>2.</b> ")
                        +getResources().getString(R.string.instructtwo)
                        +Html.fromHtml("<br><br>")
                         +Html.fromHtml("<b>3.</b> ")
                        +getResources().getString(R.string.instructthree)
                        +Html.fromHtml("<br><br>")
                       + Html.fromHtml("<b>4.</b> ")
                        +getResources().getString(R.string.instructfour)
                             );

    }

    public void updateAfterCruze(final boolean adicionar, final List<Integer> listaStamps){
        String action = (adicionar) ?  getResources().getString(R.string.add) :
                getResources().getString(R.string.delete);

        int cantidad= listaStamps.size();

        String tittleDialog = (adicionar) ?
                            action+" "+cantidad+" "+getResources().getString(R.string.faltantes) :
                            action+" "+cantidad+" "+getResources().getString(R.string.repetidas);

        new AlertDialog.Builder(this)
                .setMessage(tittleDialog)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       transfer.donateReceibe(adicionar, listaStamps);
                        Toast.makeText(CompareActivity.this, getResources().getString(R.string.changeSuccess), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(CompareActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", null)
                .show();

    }

    public  void CALLHTPP(final String typeRequest){
        //////////////////////////////////HTTP REQUEST /////////////////////////////////////////////
        eventoReq = new StringRequest(Request.Method.POST,JsonURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try{
                            JSONArray respInJson= new JSONArray(response);

                            // Parsea json
                            for (int i = 0; i < respInJson.length(); i++) {

                                    try {
                                        JSONObject obj = respInJson.getJSONObject(i);
                                        if(typeRequest.equals("compare")){

                                            int ih=obj.getInt("inhand");

                                            if(ih != -1){
                                               inhand.add(obj.getInt("inhand"));
                                            }else{
                                                errorPin();
                                                supraCompare=false;
                                            }

                                        }else{


                                            supraGenerate =obj.getString("fecha");
                                            supraPin=obj.getString("pin");
                                            transfer.updateFechaPin(false, supraGenerate);
                                            transfer.updateFechaPin(true,supraPin);

                                            txtPin.setText(supraPin);
                                            statusPin.setText(supraGenerate);
                                            supraExistPin=true;


                                        }

                                    } catch (JSONException e) {
                                        Log.v("JSON | ♦♦♦ | ERROR |", e.getMessage());
                                    }



                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                eventoReq.cancel();
                Toast.makeText(CompareActivity.this, getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(CompareActivity.this, MainActivity.class);
                startActivity(intent);

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("req", "1");
                params.put("typeReq", typeRequest);

                if(supraExistPin && typeRequest.equals("update")){
                    params.put("pin", supraPin);
                    parametros=transfer.getUrlGetChanges();
                    transfer.deleteChanges();
                }

                params.put("getPin", parametros);


                return params;
            }
        };

        int MY_SOCKET_TIMEOUT_MS=10000;
        eventoReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(eventoReq);

        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener < Object > () {
            @Override
            public void onRequestFinished(Request < Object > request) {
                btnpin.setEnabled(true);
                btnCompare.setEnabled(true);
                Dialogo();
                queue.getCache().clear();
                if(supraCompare){
                    Comparar();
                }
            }
        });
    }
    public void loadInterstitial(){
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        adRequestInters = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequestInters);
    }



    private void Comparar() {

        listRecibe.clear();
        listDonate.clear();
        int stampNumber = 0;

        int countDonate=0,countRecibe=0;
        //////RECIBIR
        String cruzeAmigo="";
        for (Integer amigo: inhand){

            if(stampNumber ==0){
                }else{
                    if(amigo>1 && amigo > inhandLocal.get(stampNumber-1) && inhandLocal.get(stampNumber-1)==0){
                        cruzeAmigo = cruzeAmigo+"#"+stampNumber+" | ";
                        listRecibe.add(stampNumber);
                        countRecibe++;
                    }
                }
            stampNumber++;
            }

        //////DONAR
        stampNumber=0;
        String cruzeLocal="";

        for (Integer amigo: inhand){

            if(stampNumber ==0){
            }else{

                if(amigo==0  && inhandLocal.get(stampNumber-1)>1){
                    cruzeLocal = cruzeLocal+"#"+stampNumber+" | ";
                    listDonate.add(stampNumber);
                    countDonate++;
                }
            }
            stampNumber++;
        }


        if(countDonate>0){
            txtAmigoFaltantes.setText(cruzeLocal);
            btnDonarTodas.setVisibility(View.VISIBLE);
        }else{
            btnDonarTodas.setVisibility(View.INVISIBLE);
            txtAmigoFaltantes.setText(getResources().getString(R.string.nothing));
        }

        if(countRecibe>0){
            txtLocalfaltantes.setText(cruzeAmigo);
            btnObtenerTodas.setVisibility(View.VISIBLE);
        }else{
            btnObtenerTodas.setVisibility(View.INVISIBLE);
            txtLocalfaltantes.setText(getResources().getString(R.string.nothing));
        }



    }

    public void Dialogo() {
        if(!supraDialog && !parametros.equals(pinAmigo.getText().toString())){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Notice:");
        builder.setMessage(getResources().getString(R.string.msjPin));
// Add the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
        supraDialog = true;
        }
    }

    public void errorPin() {

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Error:");
            builder.setMessage(getResources().getString(R.string.notPin));
// Add the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });


            AlertDialog dialog = builder.create();
            dialog.show();
            supraDialog = true;
        }

}
