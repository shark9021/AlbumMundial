package com.projectmemory.agmdevelop.mundial2018;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.projectmemory.agmdevelop.mundial2018.Utils.AdapterGrid;
import com.projectmemory.agmdevelop.mundial2018.Utils.Transfer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    TextView  dialogStamptxt, dialogInhandtxt, txFaltan, txTotal, txRepes;
    ImageView dialogimgPlus, dialogimgRest;
    AdapterGrid itemsAdapter;
    String data = "";
    GridView gridView;
    List < String > stapmNumber;
    List < Integer > stampInhand;
    EditText txtFilter;
    String supraStamp, supratextFilter = "";
    Transfer transfer;
    int supraInhand;
    ImageView ftTodas, ftRepetidas, ftFaltantes, ftok;
    Button btncompare;
    int supraINTERSTITIAL=0;
    AdView mAdView;
    InterstitialAd mInterstitialAd;
    AdRequest adRequestInters;
    ImageView imgsearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        transfer = new Transfer(this);

        ////////////////////////////////////////ADS////////////////////////////////////////////////
        MobileAds.initialize(this,
                "ca-app-pub-7375783119283606~7534484409");
        mAdView= new AdView(this);
        mAdView = findViewById(R.id.adView);
         AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mInterstitialAd= new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7375783119283606/9422281141");
        loadInterstitial();

        ///////////////////////////////////////////////////////////////////////////////////////////


        stapmNumber = new ArrayList < String > ();
        stampInhand = new ArrayList < Integer > ();

        stapmNumber = transfer.getStampsNumbers();
        stampInhand = transfer.getStampsinHands();


        gridView = findViewById(R.id.gridview);
        txtFilter = findViewById(R.id.txtfilter);

        ftFaltantes = findViewById(R.id.imgfaltantes);
        ftok = findViewById(R.id.imgok);
        ftRepetidas = findViewById(R.id.imgrepetidas);
        ftTodas = findViewById(R.id.imgtodas);

        txFaltan = findViewById(R.id.txfaltan);
        txTotal = findViewById(R.id.txttotal);
        txRepes = findViewById(R.id.txtrepe);

        imgsearch = findViewById(R.id.imgsearch);

        imgsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtFilter.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(MainActivity.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txtFilter, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        btncompare = findViewById(R.id.btncompare);

        btncompare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CompareActivity.class);
                startActivity(intent);
            }
        });

        loadData();

        itemsAdapter = new AdapterGrid(this, R.layout.grid_stamp, stapmNumber, stampInhand);
        gridView.setAdapter(itemsAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView < ? > adapterView, View view, int pos, long arg2) {
                supraStamp = (String) adapterView.getItemAtPosition(pos);
                supraInhand = itemsAdapter.getInhandTouched(pos);
                dialogStamp();
                supraINTERSTITIAL++;
            }
        });


        ////////////////////////////////// FILTERS /////////////////////////////////////////////

        txtFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                itemsAdapter.filtrar(charSequence.toString());
                supratextFilter = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ftRepetidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemsAdapter.filtrarCantidad(2);
                MostrarInterstitial();
            }
        });

        ftTodas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemsAdapter.mostrarTodas();
                txtFilter.setText("");
                MostrarInterstitial();
            }
        });

        ftok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemsAdapter.filtrarCantidad(1);
                MostrarInterstitial();
            }
        });


        ftFaltantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemsAdapter.filtrarCantidad(3);
                MostrarInterstitial();
            }
        });

    }

    private void loadData() {
        txTotal.setText("" + transfer.getData(1));
        txRepes.setText("" + transfer.getData(2));
        txFaltan.setText("" + transfer.getData(3));
    }


    public void dialogStamp() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View customView = LayoutInflater.from(this).inflate(
                R.layout.dialog_estamp, null, false);


        builder.setView(customView);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                supraINTERSTITIAL++;
                dialog.dismiss();

            }
        });

        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                stampInhand.clear();
                stapmNumber.clear();

                stapmNumber = transfer.getStampsNumbers();
                stampInhand = transfer.getStampsinHands();

                itemsAdapter.updateGrid(stapmNumber, stampInhand, supratextFilter);
                loadData();
                MostrarInterstitial();
            }
        });


        final AlertDialog mdialog = builder.show();


        dialogStamptxt = customView.findViewById(R.id.txtiddialog);
        dialogimgPlus = customView.findViewById(R.id.imgadd);
        dialogimgRest = customView.findViewById(R.id.imgrest);
        dialogInhandtxt = customView.findViewById(R.id.txtinhanddialog);
        dialogInhandtxt.setText("" + supraInhand);


        dialogimgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transfer.updateStamp(supraStamp, true);
                supraInhand = supraInhand + 1;
                dialogInhandtxt.setText("" + supraInhand);
                supraINTERSTITIAL++;

                if(supraINTERSTITIAL==5) {
                    if(!mInterstitialAd.isLoaded())
                        loadInterstitial();
                }

            }
        });

        dialogimgRest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (supraInhand > 0) {
                    transfer.updateStamp(supraStamp, false);
                    supraInhand = supraInhand - 1;
                    dialogInhandtxt.setText("" + supraInhand);
                    supraINTERSTITIAL++;

                    if(supraINTERSTITIAL==5) {
                        if(!mInterstitialAd.isLoaded())
                            loadInterstitial();
                    }
                }

            }
        });

        dialogStamptxt.setText(supraStamp);
    }

    public void loadInterstitial(){
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            adRequestInters = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequestInters);
    }

    public void MostrarInterstitial(){

        if(supraINTERSTITIAL<12){
            if(supraINTERSTITIAL==5) {
                if(!mInterstitialAd.isLoaded())
                loadInterstitial();
            }
            supraINTERSTITIAL++;
        }else{
            if(!mInterstitialAd.isLoaded()){
                loadInterstitial();
                supraINTERSTITIAL=7;
            }else{
                mInterstitialAd.show();
                supraINTERSTITIAL=0;
            }


        }

    }
}