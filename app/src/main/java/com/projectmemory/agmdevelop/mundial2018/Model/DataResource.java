package com.projectmemory.agmdevelop.mundial2018.Model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.projectmemory.agmdevelop.mundial2018.Utils.Utils;

public class DataResource extends SQLiteOpenHelper {

    public DataResource(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(Utils.CREATE_TABLE_INVENTORY);
        db.execSQL(Utils.CREATE_TABLE_PIN);
        db.execSQL(Utils.CREATE_TABLE_CHANGES);
        db.execSQL(Utils.CREATE_TABLE_FLAG_COMPARE);

        for (int i=1 ; i < 683; i++){
            db.execSQL("INSERT INTO INVENTORY (estamp, inhand) VALUES('"+i+"', 0)");
        }
        db.execSQL("INSERT INTO PIN (pin, generated) VALUES('NN', 'NN')");
        db.execSQL("INSERT INTO FLAG_COMPARE (status) VALUES(0)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
